# -*- coding: utf-8 -*-

#  Copyright © 2008, 2011  B. Clausius <barcc@gmx.de>
#  Copyright © 2007  P. Henrique Silva <ph.silva@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gedit_pylint import PylintInstance
import config

from gi.repository import GObject
from gi.repository import Gedit
from gi.repository import PeasGtk


class PylintAppActivatable(GObject.Object, Gedit.AppActivatable):
    __gtype_name__ = "PylintAppActivatable"
    app = GObject.property(type=Gedit.App)
    
    def do_activate(self):
        PylintWindowActivatable.config = config.Config()
        
    def do_deactivate(self):
        PylintWindowActivatable.config = None
        
        
class PylintWindowActivatable(GObject.Object, Gedit.WindowActivatable, PeasGtk.Configurable):
    __gtype_name__ = "PylintWindowActivatable"
    window = GObject.property(type=Gedit.Window)
    config = None
    
    def __init__(self):
        GObject.Object.__init__(self)
        self.instance = None
        assert self.config
        
    def do_activate(self):
        self.instance = PylintInstance(self.window, self.config)

    def do_deactivate(self):
        self.instance.deactivate()
        self.instance = None

    def do_update_state(self):
        self.instance.update_ui()

    def do_create_configure_widget(self):
        return self.config.create_widget()
        

