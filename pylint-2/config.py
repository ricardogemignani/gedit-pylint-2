# -*- coding: utf-8 -*-

#  Copyright © 2008, 2011  B. Clausius <barcc@gmx.de>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from gi.repository import GObject
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Gio


class Config (GObject.GObject):
    _ui_file = os.path.join(os.path.dirname(__file__) ,'config.ui')
    __gsignals__ = {
        'update-tags': (GObject.SignalFlags.RUN_FIRST,
                                    None,
                                    ()),
    }
    
    def __init__(self):
        super(Config, self).__init__()
        
        self.dialog = None
        
        self.word_errors = True
        self.line_errors = True
        self.hide_unparsed = True
        self.parse_errors = True #TODO: key + ui
        self.color_highlight = "#ffc0c0"
        self.color_button = None
        self.pylint_path = 'pylint'
        
        self.settings = Gio.Settings.new('org.gnome.gedit.plugins.pylint2')
        
        self.word_errors = self.settings.get_boolean('word-errors')
        self.line_errors = self.settings.get_boolean('line-errors')
        self.hide_unparsed = self.settings.get_boolean('hide-unparsed')
        self.color_highlight = self.settings.get_string('color-highlight') or self.color_highlight
        self.pylint_path = self.settings.get_string('pylint-path')

        self.settings.connect('changed', self.on_settings_changed)
        
    def create_widget(self):
        builder = Gtk.Builder()
        builder.add_from_file(self._ui_file)
        builder.connect_signals(self)
        
        button = builder.get_object("checkbutton_worderrors")
        self.settings.bind('word-errors', button, 'active', Gio.SettingsBindFlags.DEFAULT)
        
        button = builder.get_object("checkbutton_lineerrors")
        self.settings.bind('line-errors', button, 'active', Gio.SettingsBindFlags.DEFAULT)

        button = builder.get_object("checkbutton_hide_unparsed")
        self.settings.bind('hide-unparsed', button, 'active', Gio.SettingsBindFlags.DEFAULT)
        
        self.color_button = builder.get_object("colorbutton_highlight")
        self.color_button.set_color(Gdk.color_parse(self.color_highlight))

        self.entry_pylint_path = builder.get_object("entry_pylint_path")
        self.entry_pylint_path.set_text(self.pylint_path)
        
        return builder.get_object("widget_config")
        
    def create_dialog(self, window):
        if self.dialog:
            return self.dialog
            
        self.dialog = Gtk.Dialog(title='gedit pylint', parent=window,
                                flags=Gtk.DialogFlags.DESTROY_WITH_PARENT,
                                buttons=('gtk-close',0))
        self.dialog.get_content_area().add(self.create_widget())
        self.dialog.connect('delete-event', self.on_dialog_config_delete_event)
        self.dialog.connect('response', self.on_dialog_config_delete_event)
        return self.dialog
        
    def on_settings_changed(self, settings, unused_key):
        self.word_errors = settings.get_boolean('word-errors')
        self.line_errors = settings.get_boolean('line-errors')
        self.hide_unparsed = settings.get_boolean('hide-unparsed')
        self.color_highlight = settings.get_string('color-highlight')
        if self.color_button and self.color_highlight != self.color_button.get_color().to_string():
            self.color_button.set_color(Gdk.color_parse(self.color_highlight))
        self.pylint_path = settings.get_string('pylint-path')

        self.emit('update-tags')
        
    def on_colorbutton_highlight_color_set(self, button):
        color_highlight = button.get_color().to_string()
        if self.color_highlight != color_highlight:
            self.color_highlight = color_highlight
            self.settings.set_string('color-highlight', color_highlight)

    def on_entry_pylint_path_changed(self, entry):
        self.pylint_path = entry.get_text()
        self.settings.set_string('pylint-path', entry.get_text())
        
    def on_dialog_config_delete_event(self, *args):
        self.color_button = None
        self.dialog.destroy()
        self.dialog = None
        

