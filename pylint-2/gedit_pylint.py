# -*- coding: utf-8 -*-

#  Gedit pylint plugin
#
#  Copyright © 2008, 2010-2012  B. Clausius <barcc@gmx.de>
#  Copyright © 2007-2008  P. Henrique Silva <ph.silva@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#    Contributor(s):
#        Francisco J. Jordano Jimenez <arcturus@ardeenelinfierno.com>
#
#    pylint results parser based on code by Christopher Lenz
#    Copyright © 2005  Christopher Lenz
#

import subprocess
import os.path
import tempfile
import re

from gi.repository import Gtk
from gi.repository import Pango


PYTHON_MIME_TYPE = 'text/x-python'
PYTHON_LANG_NAME = 'Python'


class PylintResultsModel (Gtk.ListStore):
    def __init__(self):
        super(PylintResultsModel, self).__init__(str, int, str, str)

    def add(self, msg):
        self.append([msg.stock_id, msg.lineno, msg.message, msg.msg_type])
        

class PylintResultsView (Gtk.TreeView):
    def __init__(self):
        super(PylintResultsView, self).__init__()
        
        for title, cellrenderer, attrname, attrno in [
                ("Type", Gtk.CellRendererPixbuf, 'stock-id', 0),
                ("Line", Gtk.CellRendererText, 'text', 1),
                ("Number", Gtk.CellRendererText, 'text', 3),
                ("Message", Gtk.CellRendererText, 'text', 2),
                ]:
            col = Gtk.TreeViewColumn(title)
            cell = cellrenderer()
            col.pack_start(cell, True)
            col.add_attribute(cell, attrname, attrno)
            col.set_sort_column_id(attrno)
            self.append_column(col)
            

class PylintResultsPanel (object):
    _ui_file = os.path.join(os.path.dirname(__file__) ,'gedit_pylint.ui')

    def __init__(self, builder):
        builder.add_from_file(self._ui_file)
        self.widget = builder.get_object("hbox_panel")
        scrolledwindow = builder.get_object("scrolledwindow_results")
        self.checkbutton_highlight = builder.get_object("checkbutton_highlight")
        
        self.treeview = PylintResultsView()
        scrolledwindow.add(self.treeview)
        self.treeview.show()
        self.widget.show()

    def set_model(self, model):
        self.treeview.set_model(model)
        

class PylintMessage (object):
    def __init__(self, doc, msg_type, category, lineno, message, method, tag):
        self._doc = doc
        self._msg_type = msg_type
        self._category = category
        self._lineno = lineno
        self._message = message
        self._method = method
        self._tag    = tag
        self._start_iter = None
        self._end_iter   = None
        self._stock_id = self._get_stock_id(category)

    def _get_stock_id(self, category):
        if category == "error":
            return Gtk.STOCK_DIALOG_ERROR
        elif category == "warning":
            return Gtk.STOCK_DIALOG_WARNING
        elif category in ["convention", "refactor"]:
            return Gtk.STOCK_DIALOG_INFO
        else:
            return Gtk.STOCK_NO

    def setWordBounds(self, start, end):
        self._start_iter = start
        self._end_iter = end

    doc = property(lambda self: self.__doc)

    msg_type = property(lambda self: self._msg_type)
    category = property(lambda self: self._category)

    lineno = property(lambda self: self._lineno)
    message = property(lambda self: self._message)

    method = property(lambda self: self._method)
    tag = property(lambda self: self._tag)

    start = property(lambda self: self._start_iter)
    end = property(lambda self: self._end_iter)

    stock_id = property(lambda self: self._stock_id)
    
    
class PylintInstance (object):
    def __init__(self, window, config):
        self._window = window
        self._config = config
        self._merge_id = None
        self._action_group = None
        self._panel = None
        self._result_models = {None: PylintResultsModel()}
        self._errors = {}
        self._word_error_tags = {}
        self._line_error_tags = {}
        self._status_id = 98

        self._insert_panel()
        self._insert_menu()
        self._attach_events()

    def deactivate(self):
        self._remove_menu()
        self._remove_panel()

        # "destroy objects"
        self._merge_id = None
        self._action_group = None
        self._panel = None
        self._result_models = {}
        self._errors = {}

        for doc in self._window.get_documents():
            self._remove_tags(doc)
        self._word_error_tags = {}
        self._line_error_tags = {}

        self._window = None
        self._config = None

    def update_ui(self):
        doc = self._window.get_active_document()
        self._action_group.set_sensitive(doc != None)
        if doc in self._result_models:
            self._panel.set_model(self._result_models[doc])
        else:
            self._panel.set_model(self._result_models[None])

    def _insert_panel(self):
        builder = Gtk.Builder()
        self._panel = PylintResultsPanel(builder)
        builder.connect_signals(self)
        self._panel.treeview.connect("row-activated", self.on_row_activated)

        image = Gtk.Image()
        image.set_from_icon_name('gnome-mime-text-x-python', Gtk.IconSize.MENU)

        bottom_panel = self._window.get_bottom_panel()
        bottom_panel.add_item(self._panel.widget, 'PylintResultsPanel', 'Pylint Results', image)

    def _remove_panel(self):
        bottom_panel = self._window.get_bottom_panel()
        bottom_panel.remove_item(self._panel.widget)

    def _insert_menu(self):
        manager = self._window.get_ui_manager()

        self._action_group = Gtk.ActionGroup("GeditPylintPluginActions")
        self._action_group.set_translation_domain('pylint')
        self._action_group.add_actions([('Pylint', None,
                                         _('Pylint'),
                                         None,
                                         _('Run Pylint for the current document'),
                                         self.on_action_pylint_activate)])
        manager.insert_action_group(self._action_group, -1)

        ui_str = """<ui>
                    <menubar name="MenuBar">
                     <menu name="ToolsMenu" action="Tools">
                      <placeholder name="ToolsOps_2">
                        <menuitem name="Pylint" action="Pylint"/>
                       </placeholder>
                      </menu>
                     </menubar>
                    </ui>"""
        self._merge_id = manager.add_ui_from_string(ui_str)

    def _remove_menu(self):
        manager = self._window.get_ui_manager()
        manager.remove_ui(self._merge_id)
        manager.remove_action_group(self._action_group)
        manager.ensure_update()

    def _add_tags(self, doc):
        self._word_error_tags[doc] = doc.create_tag("pylint-word-error",
                                                    underline=Pango.Underline.ERROR)
        self._line_error_tags[doc] = doc.create_tag("pylint-line-error",
                                                    background=self._config.color_highlight)

    def _remove_tags(self, doc):
        if self._word_error_tags.has_key(doc):
            start, end = doc.get_bounds()
            doc.remove_tag(self._word_error_tags[doc], start, end)
            doc.remove_tag(self._line_error_tags[doc], start, end)

    def _attach_events(self):
        self._window.connect("tab_added", self.on_tab_added)
        self._window.connect("tab_removed", self.on_tab_removed)
        self._config.connect('update-tags', self.on_config_update_tags)

    def on_tab_added(self, window, tab):
        doc = tab.get_document()
        self._errors[doc] = []
        # add tags to document
        self._add_tags(doc)

    def on_tab_removed(self, window, tab):
        doc = tab.get_document()
        if self._result_models.has_key(doc):
            self._result_models[doc] = None
            del self._result_models[doc]
            self._errors[doc] = None
            del self._errors[doc]
            self._remove_tags(doc)
            
    def on_action_pylint_activate(self, action):
        doc = self._window.get_active_document()

        # only run on Python documents (not perfect, maybe use mime type too)
        if not self._is_python_document(doc):
            # flash_message is only avaiable on gedit >= 2.17.5
            status = self._window.get_statusbar()
            try:
                status.flash_message(self._status_id,
                                     "%s is not a Python file." % doc.get_uri_for_display())
            except AttributeError:
                print "%s is not a Python file." % doc.get_uri_for_display()
            return
        
        doc_file = doc.get_location()
        in_filename = None
        if not doc.get_modified() and doc_file:
            in_filename = doc_file.get_path()
            in_tmpFile = None
        if in_filename is None:
            # get iters and text
            start, end = doc.get_bounds()
            text = doc.get_text(start, end)

            # save to a temporary
            in_tmpFile = tempfile.NamedTemporaryFile("w+", suffix="-gedit-pylint")
            in_tmpFile.write(text)
            in_tmpFile.flush()
            in_filename = in_tmpFile.name
        
        # run pylint from the parentdir of the package, so that pylint can find modules there
        cwd = doc_file.get_parent()
        while cwd.get_child('__init__.py').query_exists(None):
            cwd = cwd.get_parent()
        cwd = cwd.get_path()
        
        pylint_path = self._config.pylint_path or 'pylint'
        pylint_path = pylint_path.replace('{cwd}', cwd)
        if not os.path.exists(pylint_path):
            pylint_path = 'pylint'
        
        process = subprocess.Popen(
                    (pylint_path, "--reports=no", "--msg-template='{path}:{line}: [{msg_id}, {obj}] {msg}'",
                        in_filename),
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    cwd=cwd)
        stdout, stderr = process.communicate()
        err_lines = stdout.splitlines()
        
        # cleanup previous run errors
        self._remove_tags(doc)
        if doc not in self._result_models:
            self._result_models[doc] = PylintResultsModel()
            self._panel.set_model(self._result_models[doc])
        self._result_models[doc].clear()
        
        self._panel.checkbutton_highlight.set_active(True)

        # display results
        self._errors[doc] = self._parse_errors(err_lines)
        self._hightlight_errors(doc)
        self._add_to_results(doc)
        
        if in_tmpFile:
            # the file is deleted as soon as it is closed
            in_tmpFile.close()

    def on_row_activated(self, treeview, row, column):
        model = treeview.get_model()
        tree_iter = model.get_iter(row)
        doc = self._window.get_active_document()
        line = model.get_value(tree_iter, 1) - 1
        doc.goto_line(line)
        view = self._window.get_active_view()
        text_iter = doc.get_iter_at_line(line)
        view.scroll_to_iter(text_iter, 0.25, False, 0.5, 0.5)
        view.grab_focus()
        
    def on_button_check_clicked(self, *args):
        self.on_action_pylint_activate(None)

    def on_checkbutton_highlight_toggled(self, button):
        doc = self._window.get_active_document()
        if button.get_active():
            self._hightlight_errors(doc)
        else:
            self._remove_tags(doc)
    
    def on_button_config_clicked(self, *args):
        dialog = self._config.create_dialog(self._window)
        dialog.run()
        
    _msg_re = re.compile(r'^(?P<file>.+):(?P<line>\d+): '
                        r'\[(?P<type>[A-Z]\d*)(?:,(?P<method>[^\]]+))?\] '
                        r'(?P<msg>.*)$')
    _msg_categories = dict(W='warning', E='error', C='convention', R='refactor')
    
    def _parse_errors(self, err_lines):
        errors = []

        for line in err_lines:
            match = self._msg_re.match(line)

            if match:
                msg_type = match.group('type')
                category = self._msg_categories.get(msg_type[0])
                if len(msg_type) == 1:
                    msg_type = None
                lineno = int(match.group('line'))
                method = match.group('method')
                msg = match.group('msg') or ''
                tag = self._parse_tag(msg_type, msg)
            else:
                if self._config.hide_unparsed:
                    continue
                msg_type = None #'E0'
                category = 'fatal'
                lineno = 1
                method = 'None'
                msg = 'plugin failed to parse pylint-error: '+line.strip()
                tag = ''
            
            if msg_type is not None or self._config.parse_errors:
                errors.append(PylintMessage(self._window.get_active_document,
                                            msg_type,
                                            category,
                                            lineno,
                                            msg,
                                            method,
                                            tag or ''))
        return errors

    def _hightlight_errors(self, doc):
        for err in self._errors[doc]:
            if err.category != "error":
                continue

            start = doc.get_iter_at_line(err.lineno - 1)
 
            end = doc.get_iter_at_line(err.lineno - 1)
            end.forward_to_line_end()

            # apply tag to word, if any
            if err.tag:
                try:
                    match_start, match_end = start.forward_search(err.tag,
                                                               Gtk.TextSearchFlags.TEXT_ONLY,
                                                               end)
                    if self._config.word_errors:
                        doc.apply_tag(self._word_error_tags[doc], match_start, match_end)
                except TypeError:
                    pass

            # apply tag to entire line
            if self._config.line_errors:
                doc.apply_tag(self._line_error_tags[doc], start, end)
                

    def _add_to_results(self, doc):
        for err in self._errors[doc]:
            self._result_models[doc].add(err)

    def _parse_tag(self, msg_type, msg):
        tag = ''
        if msg_type == "E0602":
            tag = msg[msg.find("'")+1: msg.rfind("'")]
        return tag
    
    def _is_python_document(self, doc):
        if doc.get_mime_type() == PYTHON_MIME_TYPE:
            return True
        if doc.get_language() and doc.get_language().get_name() == PYTHON_LANG_NAME:
            return True
        return False
        
    def on_config_update_tags(self, unused_config):
        for doc in self._window.get_documents():
            self._remove_tags(doc)
            self._hightlight_errors(doc)
            self._line_error_tags[doc].props.background = self._config.color_highlight
    
    
