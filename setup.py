#!/usr/bin/python
# -*- coding: utf-8 -*-

#  Copyright © 2010-2012  B. Clausius <barcc@gmx.de>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


from glob import glob
import distutils.core

distutils.core.setup(
    name='gedit-pylint-2',
    version='2.1',
    license='GPL-3',
    author='B Clausius',
    author_email='barcc@gmx.de',
    description='Pylint static analysis support',
    url='https://launchpad.net/gedit-pylint-2',

    data_files=[
                ('lib/gedit/plugins/', ['pylint-2.plugin']),
                ('share/glib-2.0/schemas/', glob('*.gschema.xml')),
                ('lib/gedit/plugins/pylint-2/', glob('pylint-2/*.py')),
                ('lib/gedit/plugins/pylint-2/', glob('pylint-2/*.ui')),
               ],
    )

